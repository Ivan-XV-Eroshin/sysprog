//
// Created by PC03 on 23.09.2019.
//

#ifndef UNTITLED2_ENTITY_H
#define UNTITLED2_ENTITY_H


#include <string>
#include <fstream>

struct Manufacturing {
    unsigned long long product_id;
    std::string group;
    std::string date;
    int production_volume;
    int metal_consumption;
    Manufacturing *next;
    Manufacturing *last;
};


#endif //UNTITLED2_ENTITY_H
