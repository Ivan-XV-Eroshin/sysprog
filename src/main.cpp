#include <iostream>
#include "Entity.h"

int main() {
    Manufacturing* manufacturing = new Manufacturing();
    manufacturing->date = "11.11.1989";
    manufacturing->group = "boxes";
    manufacturing->metal_consumption = 2000;
    manufacturing->product_id = 1000;
    manufacturing->production_volume = 40000;
    return 0;
}